#!/usr/bin/python
#################################################################################
# FILENAME   : Viterbi.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 03/09/2013
# DESCRIPTION: 
#################################################################################

from os import system
from math import exp, log
import cPickle as pickle


#ProjectPath = "/home/brian/Desktop/MOOC/Coursera/Natural_Language_Processing/Unit 01/Homework/"
ProjectPath = "/ssd/Projects/MOOC/Coursera/Natural_Language_Processing/Unit 01/Homework/"

SRC = ProjectPath + "src/"
DATA = ProjectPath + "data/"
OUTPUT = ProjectPath + "output/"

Count_x_y = pickle.load( open( DATA + "Count_x_y.pkl", "r" ) )
Count_x = pickle.load( open( DATA + "Count_x.pkl", "r" ) )
    
N1_gram = pickle.load( open( DATA + "N1_gram.pkl", "r" ) )
N2_gram = pickle.load( open( DATA + "N2_gram.pkl", "r" ) )
N3_gram = pickle.load( open( DATA + "N3_gram.pkl", "r" ) )

word_count_by_tag  = pickle.load( open( DATA + "word_count_by_tag.pkl", "r" ) )


def e( x, v ):

    numerator =  Count_x_y.get((x,v))
    if numerator == None:
        numerator = Count_x_y.get(('_RARE_',v))

    denominator = N1_gram.get(v)

    return( float(numerator)/denominator )





x = "This is a test for uPA".split()

sent1 = "Third , consistent with these data , N - acetylcysteine reduced the stimulatory effect of HGF on stress kinase activities , while p42 / 44 mitogen activated kinase ( MAPK ) was unmodified , suggesting an involvement of c - Jun - N - terminal kinase ( JNK ) and p38 MAPK in HIF - 1 activation .".split()
sent2 = "The Sox gene family ".split()
#like HMG  box gene is  characterised  by a conserved DNA sequence encoding a domain ".split()
sent3 = "of approximately 80 amino acids which is responsible for sequence specific DNA binding .".split()



sent4 = "_RARE_ , consistent with these data , N - acetylcysteine reduced the stimulatory effect of  HGF on stress kinase activities , while _RARE_ / 44 mitogen activated kinase ( MAPK ) was _RARE_ , suggesting an involvement of c - Jun - N - terminal kinase ( JNK ) and p38 MAPK in HIF - 1 activation".split()


def viterbi( text, emission = e, n1_gram = N1_gram, n2_gram = N2_gram, n3_gram = N3_gram ):

    #log_pi = 0
    tags = []

    for t in range( len(text) ):
        pi_max = 0
        tag_max = max( N1_gram, key = N1_gram.get ) #Initialize to  most common tag
        word = text[t]

        if t == 0:
            for tag in n1_gram.keys():
                pi_next = float(n1_gram.get( (tag) ) )/sum( n1_gram.values() ) * emission( word, tag )
                if pi_next > pi_max:
                    pi_max = pi_next
                    tag_max = tag

        elif t == 1:
            for tag in n1_gram.keys():
                pi_next = float(n2_gram.get( (tags[t-1],tag) ) )/n1_gram.get( (tags[t-1]) ) * emission( word, tag ) 
                if pi_next > pi_max:
                    pi_max = pi_next
                    tag_max = tag


        else:
            for tag in n1_gram.keys():
                numerator = n3_gram.get( (tags[t-2],tags[t-1],tag) )
                denominator = n2_gram.get( (tags[t-2],tags[t-1]) )
                
                if numerator == None and denominator != None:
                    numerator = 0
                    
                elif numerator == None and denominator == None:
                    numerator = n2_gram.get( (tags[t-1],tag) )
                    denominator = n1_gram.get( (tags[t-1]) )
                
                #print "numerator:", numerator, "denominator:", denominator, "emission:", emission( word, tag ) 
                pi_next = float( numerator )/denominator  * emission( word, tag )

                if pi_next > pi_max:
                    pi_max = pi_next
                    tag_max = tag
        
        print text[t], pi_max, tag_max
        tags.append( tag_max )            

    return tags



#pi1, tags1 = viterbi( text = x )
#pi2, tags2 = viterbi( text = sent1 )
tags3 = viterbi( text = sent4 )

# Read in text file
# Construct list of sentences
# Tag each sentence
# Write tags to output


## END OF FILE
