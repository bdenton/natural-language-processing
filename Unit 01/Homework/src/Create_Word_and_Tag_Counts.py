#!/usr/bin/python
#################################################################################
# FILENAME   : Create_Word_and_Tag_Counts.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 03/09/2013
# DESCRIPTION: 
#################################################################################

from os import system
import cPickle as pickle

#ProjectPath = "/home/brian/Desktop/MOOC/Coursera/Natural_Language_Processing/Unit 01/Homework/"
ProjectPath = "/ssd/Projects/MOOC/Coursera/Natural_Language_Processing/Unit 01/Homework/"

SRC = ProjectPath + "src/"
DATA = ProjectPath + "data/"
OUTPUT = ProjectPath + "output/"

#################################################################################
# Define function that reads gene counts and n-grams into dictionaries          #
#################################################################################

def get_counts( infile ):

    count_x = dict()   # Count(x)
    count_x_y = dict() # Count(x|y), where x = Words and y = Tags

    n1_gram = dict()   # Count( y[n] )
    n2_gram = dict()   # Count( y[n-1], y[n] )
    n3_gram = dict()   # Count( y[n-2], y[n-1], y[n] )

    for line in open( infile, "r" ):

        line = line.split()
        
        if line[1] == "WORDTAG":
            # count_x_y[ ( word, tag ) ] -> count
            count_x_y[( line[3], line[2] )] = int( line[0] )
            
            # If word already in count_x increment count, otherwise add to dictionary.
            # count_x[ word ] -> count
            if count_x.get( line[3] ) != None:
                count_x[ line[3] ] = count_x[ line[3] ] + int( line[0] )
            else:
                count_x[ line[3] ] = int( line[0] )
                
                
        elif line[1] == "1-GRAM":
            n1_gram[( line[2] )] = int( line[0] )
            
        elif line[1] == "2-GRAM":
            n2_gram[( line[2], line[3] )] = int( line[0] )
            
        elif line[1] == "3-GRAM":
            n3_gram[( line[2], line[3], line[4] )] = int( line[0] )

    return count_x, count_x_y, n1_gram, n2_gram, n3_gram




#################################################################################
# Define function that replaces rare words in training data with a generic 
# value and  writes output to new file. 
#################################################################################

def rare_words_training(  infile, outfile, count_x, generic = "_RARE_", threshold = 5 ):

    rare_words = set()
    
    out = open( outfile, "w" )
    
    for line in open( infile, "r" ):
        
        if line != '\n':
            
            line = line.split()
            
            if count_x.get( line[0] ) < threshold:
                rare_words.add( line[0] )
                line[0] = generic 
                
            out.write( ' '.join( line ) + "\n" )
                
                
    out.close()

    return rare_words

#################################################################################
# Mask rare words in test document
#################################################################################


def rare_words_test(  infile, outfile, rare_words, generic = "_RARE_" ):

    out = open( outfile, "w" )
    
    for line in open( infile, "r" ):
        
        if line != '\n':

            line = line.split()
        
            if line[0] in rare_words:
                line[0] = generic 

        out.write( ' '.join( line ) + "\n" )
                
                
    out.close()


#################################################################################
# Create a dictionary that takes word x as its key and its value is a list of 
# tuples ( y, Count(x|y) ) for each y such that Count(x|y) > 0.
#################################################################################

def get_word_count_by_tag( count_x_y ):

    word_count_by_tag = dict()

    for (WORD, TAG), COUNT in count_x_y.items():

        tag_list = word_count_by_tag.get( WORD )

        if tag_list != None:
            word_count_by_tag[ WORD ].append( (TAG,COUNT) )

        else:
            word_count_by_tag[ WORD ] = [(TAG,COUNT)]

    return word_count_by_tag



#################################################################################
# Call functions to generate dictionaries                                       #
#################################################################################

# 1. Read gene.counts and populate word count and n-gram dictionaries.

Count_x, Count_x_y, N1_gram, N2_gram, N3_gram = get_counts( DATA + "gene.counts" )


# 2. Mask the low frequency words in gene.train and gene.test

RARE_WORDS = rare_words_training( infile = DATA + "gene.train", outfile = DATA + "gene.train.new", count_x = Count_x )

rare_words_test( infile = DATA + "gene.test", outfile = DATA + "gene.test.new", rare_words = RARE_WORDS )

# 3. Update gene.counts

system( "python ./lib/count_freqs.py ../data/gene.train.new > ../data/gene.counts.new" )


# 4. Update word count and n-gram dictionaries

Count_x, Count_x_y, N1_gram, N2_gram, N3_gram = get_counts( DATA + "gene.counts.new" )


# 5. Generate dictionary of tag counts for each word

word_count_by_tag = get_word_count_by_tag( count_x_y = Count_x_y )



#################################################################################
# Pickle dictionaries
#################################################################################

pickle.dump( Count_x_y, open( DATA + "Count_x_y.pkl", "w" ) )
pickle.dump( Count_x, open( DATA + "Count_x.pkl", "w" ) )

pickle.dump( N1_gram, open( DATA + "N1_gram.pkl", "w" ) )
pickle.dump( N2_gram, open( DATA + "N2_gram.pkl", "w" ) )
pickle.dump( N3_gram, open( DATA + "N3_gram.pkl", "w" ) )

pickle.dump( word_count_by_tag, open( DATA + "word_count_by_tag.pkl", "w" ) )

## END OF FILE
