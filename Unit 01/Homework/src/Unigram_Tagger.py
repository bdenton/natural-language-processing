#!/usr/bin/python
#################################################################################
# FILENAME   : Unigram_Tagger.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 03/09/2013
# DESCRIPTION: 
#################################################################################

from os import system
import cPickle as pickle
from Helper_Functions import *

ProjectPath = "/home/brian/Desktop/MOOC/Coursera/Natural_Language_Processing/Unit 01/Homework/"

SRC = ProjectPath + "src/"
DATA = ProjectPath + "data/"
OUTPUT = ProjectPath + "output/"

Count_x_y = pickle.load( open( DATA + "Count_x_y.pkl", "r" ) )
Count_x = pickle.load( open( DATA + "Count_x.pkl", "r" ) )
    
N1_gram = pickle.load( open( DATA + "N1_gram.pkl", "r" ) )
N2_gram = pickle.load( open( DATA + "N2_gram.pkl", "r" ) )
N3_gram = pickle.load( open( DATA + "N3_gram.pkl", "r" ) )

word_count_by_tag  = pickle.load( open( DATA + "word_count_by_tag.pkl", "r" ) )


def unigram_tagger( infile, outfile ):

    out = open( outfile, "w" )

    for line in open(infile, "r" ):
    
        if line != "\n":
            line = line.strip()
            out.write( line + " " + ml_tag( word = line, count_x_y = Count_x_y, n1_gram = N1_gram ) + "\n" )

        else:
            out.write( line )

    out.close()



unigram_tagger( infile = DATA + "gene.dev", outfile = OUTPUT + "gene_dev.p1.out" )
unigram_tagger( infile = DATA + "gene.test", outfile = OUTPUT + "gene_test.p1.out" )

# Check results
system( "python ./lib/eval_gene_tagger.py ../data/gene.key ../output/gene_dev.p1.out > ../output/eval.part1.txt" )

## END OF FILE
